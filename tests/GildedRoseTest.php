<?php

declare(strict_types=1);

namespace Tests;

use ApprovalTests\Approvals;
use PHPUnit\Framework\TestCase;

class GildedRoseTest extends TestCase
{
    public function testVerifyByApproval(): void
    {
        ob_start();
        $argv[0] = 'texttest_fixture.php';
        $argv[1] = 31;
        include __DIR__ .'/../fixtures/texttest_fixture.php';
        $output = ob_get_clean();

        Approvals::verifyString($output);
    }
}
