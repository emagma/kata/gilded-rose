<?php

declare(strict_types=1);

namespace GildedRose\Updater\Quality;

use GildedRose\Item;

final class AgedBrieQualityUpdater implements QualityUpdaterInterface
{
    public function update(Item $item): void
    {
        // Une fois que la date de péremption est passée, la qualité se dégrade deux fois plus rapidement
        // "Aged Brie" augmente sa qualité (quality) plus le temps passe
        $item->quality += ($item->sell_in <= 0 ? 2 : 1);

        // La qualité d'un produit n'est jamais de plus de 50
        $item->quality = min($item->quality, 50);
    }

    public function supports(Item $item): bool
    {
        return 'Aged Brie' === $item->name;
    }
}
