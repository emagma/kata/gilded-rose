<?php

declare(strict_types=1);

namespace GildedRose\Updater\Quality;

use GildedRose\Item;

final class SulfurasQualityUpdater implements QualityUpdaterInterface
{

    public function update(Item $item): void
    {
        // "Sulfuras", étant un objet légendaire, n'a pas de date de péremption et ne perd jamais en qualité (quality)
        $item->quality = 80;
    }

    public function supports(Item $item): bool
    {
        return 'Sulfuras, Hand of Ragnaros' === $item->name;
    }
}
