<?php

declare(strict_types=1);

namespace GildedRose\Updater\Quality;

use GildedRose\Item;

final class ConjuredQualityUpdater implements QualityUpdaterInterface
{

    public function update(Item $item): void
    {
        // les éléments "Conjured" voient leur qualité se dégrader de deux fois plus vite que les objets normaux
        $item->quality -= $item->sell_in <= 0 ? 4 : 2;
        $item->quality = max($item->quality, 0);
    }

    public function supports(Item $item): bool
    {
        return 'Conjured Mana Cake' === $item->name;
    }
}
