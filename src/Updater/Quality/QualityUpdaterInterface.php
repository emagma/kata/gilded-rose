<?php

declare(strict_types=1);

namespace GildedRose\Updater\Quality;

use GildedRose\Item;

interface QualityUpdaterInterface
{
    public function update(Item $item): void;

    public function supports(Item $item): bool;
}