<?php

namespace GildedRose\Updater\Quality;

use GildedRose\Item;

class DefaultQualityUpdater implements QualityUpdaterInterface
{
    public function update(Item $item): void
    {
        $qualityStep = $item->sell_in > 0 ? 1 : 2;
        $item->quality = min(max($item->quality - $qualityStep, 0), 50);
    }

    public function supports(Item $item): bool
    {
        return in_array($item->name, [
           '+5 Dexterity Vest',
           'Elixir of the Mongoose'
        ]);
    }
}