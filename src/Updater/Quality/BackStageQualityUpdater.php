<?php

declare(strict_types=1);

namespace GildedRose\Updater\Quality;

use GildedRose\Item;

final class BackStageQualityUpdater implements QualityUpdaterInterface
{
    public function update(Item $item): void
    {
        switch (true) {
            // La qualité tombe à 0 après le concert
            case $item->sell_in <= 0:
                $item->quality = 0;
                break;
            // La qualité augmente de 3 quand il reste 5 jours ou moins
            case $item->sell_in <= 5:
                $item->quality += 3;
                break;
            // La qualité augmente de 2 quand il reste 10 jours ou moins
            case $item->sell_in <= 10:
                $item->quality += 2;
                break;
            // "Backstage passes", comme le "Aged Brie", augmente sa qualité (quality) plus le temps passe (sellIn)
            default:
                $item->quality++;
        }

        // La qualité d'un produit n'est jamais de plus de 50
        $item->quality = min($item->quality, 50);
    }

    public function supports(Item $item): bool
    {
        return 'Backstage passes to a TAFKAL80ETC concert' === $item->name;
    }
}
