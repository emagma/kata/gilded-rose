<?php

declare(strict_types=1);

namespace GildedRose\Updater;

use GildedRose\Item;
use GildedRose\Updater\Exception\QualityUpdaterNotFoundException;
use GildedRose\Updater\Exception\SellInUpdaterNotFoundException;
use GildedRose\Updater\Quality\AgedBrieQualityUpdater;
use GildedRose\Updater\Quality\BackStageQualityUpdater;
use GildedRose\Updater\Quality\DefaultQualityUpdater;
use GildedRose\Updater\Quality\ConjuredQualityUpdater;
use GildedRose\Updater\Quality\QualityUpdaterInterface;
use GildedRose\Updater\Quality\SulfurasQualityUpdater;
use GildedRose\Updater\SellIn\DefaultSellInUpdater;
use GildedRose\Updater\SellIn\SulfurasSellInUpdater;
use GildedRose\Updater\SellIn\SellInUpdaterInterface;

final class ItemUpdater
{
    private ItemUpdaterInterface $legacyUpdater;

    /** @var QualityUpdaterInterface[] */
    private $qualityUpdaters;

    /** @var SellInUpdaterInterface[] */
    private $sellInUpdaters;

    public function __construct()
    {
        $this->legacyUpdater = new LegacyItemUpdater();
        $this->qualityUpdaters = [
            new SulfurasQualityUpdater(),
            new AgedBrieQualityUpdater(),
            new BackStageQualityUpdater(),
            new DefaultQualityUpdater(),
            new ConjuredQualityUpdater(),
        ];
        $this->sellInUpdaters = [
            new SulfurasSellInUpdater(),
            new DefaultSellInUpdater(),
        ];
    }

    public function update(Item $item): void
    {
        try {
            $qualityUpdater = $this->findQualityUpdater($item);
            $sellInUpdater = $this->findSellInUpdater($item);

            $qualityUpdater->update($item);
            $sellInUpdater->update($item);
        } catch (SellInUpdaterNotFoundException | QualityUpdaterNotFoundException $updaterNotFoundException) {
            $this->legacyUpdater->update($item);
        }
    }

    private function findQualityUpdater(Item $item): QualityUpdaterInterface
    {
        /** @var QualityUpdaterInterface $qualityUpdater */
        foreach ($this->qualityUpdaters as $qualityUpdater) {
            if ($qualityUpdater->supports($item))
                return $qualityUpdater;
        }

        throw new QualityUpdaterNotFoundException();
    }

    private function findSellInUpdater(Item $item): SellInUpdaterInterface
    {
        /** @var SellInUpdaterInterface $sellInUpdater */
        foreach ($this->sellInUpdaters as $sellInUpdater) {
            if ($sellInUpdater->supports($item))
                return $sellInUpdater;
        }

        throw new SellInUpdaterNotFoundException();
    }
}
