<?php

declare(strict_types=1);

namespace GildedRose\Updater\Exception;

final class QualityUpdaterNotFoundException extends \LogicException
{
}
