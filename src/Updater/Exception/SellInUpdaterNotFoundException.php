<?php

declare(strict_types=1);

namespace GildedRose\Updater\Exception;

final class SellInUpdaterNotFoundException extends \LogicException
{
}
