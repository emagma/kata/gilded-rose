<?php

declare(strict_types=1);

namespace GildedRose\Updater\SellIn;

use GildedRose\Item;

final class SulfurasSellInUpdater implements SellInUpdaterInterface
{

    public function update(Item $item): void
    {
        // "Sulfuras" est un objet légendaire et comme tel sa qualité est de 80 et elle ne change jamais
    }

    public function supports(Item $item): bool
    {
        return 'Sulfuras, Hand of Ragnaros' === $item->name;
    }
}
