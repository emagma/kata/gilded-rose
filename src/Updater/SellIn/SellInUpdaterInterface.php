<?php

declare(strict_types=1);

namespace GildedRose\Updater\SellIn;

use GildedRose\Item;

interface SellInUpdaterInterface
{
    public function update(Item $item): void;

    public function supports(Item $item): bool;
}