<?php

namespace GildedRose\Updater\SellIn;

use GildedRose\Item;

class DefaultSellInUpdater implements SellInUpdaterInterface
{
    public function update(Item $item): void
    {
        $item->sell_in--;
    }

    public function supports(Item $item): bool
    {
        return true;
    }
}