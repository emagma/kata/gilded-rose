<?php

declare(strict_types=1);

namespace GildedRose;

use GildedRose\Updater\ItemUpdater;

final class GildedRose
{
    /** @var Item[] */
    private array $items;

    private ItemUpdater $itemUpdater;

    public function __construct(array $items)
    {
        foreach ($items as $item) {
            $this->addItem($item);
        }

        $this->itemUpdater = new ItemUpdater();
    }

    public function updateQuality(): void
    {
        foreach ($this->items as $item) {
            $this->itemUpdater->update($item);
        }
    }

    private function addItem(Item $item): void
    {
        $this->items[] = $item;
    }
}
